/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userservice;

/**
 *
 * @author hhwiersma
 */
public class PointInSpace {

    int x;
    int y;
    int z;

    /**
     * Calculate Euclidean distance to other PointInSpace
     *
     * @param otherPoint
     * @return distance rounded down
     */
    int calculateDistance(PointInSpace otherPoint) {
        //afstanden berekenen
        double disX = Math.pow((this.x - otherPoint.x), 2);
        double disY = Math.pow((this.y - otherPoint.y), 2);
        double disZ = Math.pow((this.z - otherPoint.z), 2);

        //Wortel ervan nemen
        double distance = Math.sqrt(disX + disY + disZ);

        //INT terug geven
        return (int) distance;
    }

}
